---
layout: single
title: "Academic papers on FLOSS Foundations"
permalink: /resources/academic
categories:
    - resources
---

* Hunter, P., Walli, S. (2013). The Rise and Evolution of the Open Source Software Foundation. International Free and Open Source Software Law Review, 5(1). [https://doi.org/10.5033/ifosslr.v5i1.64](https://doi.org/10.5033/ifosslr.v5i1.64)
* Izquierdo, J. L. C., Cabot, J. (2018). The role of foundations in open source projects. Proceedings of the 40th International Conference on Software Engineering Software Engineering in Society (ICSE-SEIS). [https://doi.org/10.1145/3183428.3183438](https://doi.org/10.1145/3183428.3183438)
* Lindman J., Hammouda I. (2017). Investigating Relationships Between FLOSS Foundations and FLOSS Projects. In: Balaguer F., Di Cosmo R., Garrido A., Kon F., Robles G., Zacchiroli S. (eds) Open Source Systems: Towards Robust Practices. OSS 2017. IFIP Advances in Information and Communication Technology, vol 496. Springer. [https://doi.org/10.1007/978-3-319-57735-7_2](https://doi.org/10.1007/978-3-319-57735-7_2)
* Lindman, J., Hammouda, I. (2018). Support mechanisms provided by FLOSS foundations and other entities. J Internet Serv Appl 9(8). [https://doi.org/10.1186/s13174-018-0079-2](https://doi.org/10.1186/s13174-018-0079-2)
* O'Mahony, S. (2005). Nonprofit foundations and their role in community-firm software collaboration. In: Feller, J., Fitzgerald, B., Hissam, S.A., Lakhani, K.R. (eds.) Perspectives on Free and Open Source Software, pp. 393-413. The MIT Press, Cambridge. [https://doi.org/10.7551/mitpress/5326.003.0028 ](https://doi.org/10.7551/mitpress/5326.003.0028 )
* Prattico, L. (2012). Governance of Open Source Software Foundations: Who Holds the Power? In: Technology Innovation Management Review, 2(12): 37-42. [https://doi.org/10.22215/timreview/636](https://doi.org/10.22215/timreview/636)
* Riehle, D. (2010). The Economic Case for Open Source Foundations. Computer, 43(1), pp. 86-90. [https://doi.org/10.1109/MC.2010.24](https://doi.org/10.1109/MC.2010.24)
* Riehle D., Berschneider S. (2012). A Model of Open Source Developer Foundations. In: Hammouda I., Lundell B., Mikkonen T., Scacchi W. (eds) Open Source Systems: Long-Term Sustainability. OSS 2012. IFIP Advances in Information and Communication Technology, vol 378. Springer, Berlin, Heidelberg. [https://doi.org/10.1007/978-3-642-33442-9_2](https://doi.org/10.1007/978-3-642-33442-9_2)
* Schwab, B., Riehle, D., Barcomb, A. & Harutyunyan, N. (2020). The Ecosystem of openKONSEQUENZ, a User-Led Open Source Foundation. In: Ivanov V., Kruglov A., Masyagin S., Sillitti A., Succi G. (eds) Open Source Systems. OSS 2020. IFIP Advances in Information and Communication Technology, vol 582. Springer. [https://doi.org/10.1007/978-3-030-47240-5_1](https://doi.org/10.1007/978-3-030-47240-5_1)
* Weikert F., Riehle D., Barcomb A. (2019). Managing Commercial Conflicts of Interest in Open Source Foundations. In: Hyrynsalmi S., Suoranta M., Nguyen-Duc A., Tyrväinen P., Abrahamsson P. (eds) Software Business. ICSOB 2019. Lecture Notes in Business Information Processing, 370. Springer. [https://doi.org/10.1007/978-3-030-33742-1_11](https://doi.org/10.1007/978-3-030-33742-1_11)
* Xie, Zhensheng (2008). Open Source Software Foundations. Open Source Business Resource. [https://doi.org/10.22215/timreview/194](https://doi.org/10.22215/timreview/194)
