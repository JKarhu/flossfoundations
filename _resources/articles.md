---
layout: single
title: "Articles related to FLOSS Foundations"
permalink: /resources/articles
categories:
    - resources
---

* [Mozilla's trademark enforcement experience](https://lwn.net/Articles/546678/), LWN, 2013.
* [Non-profits, foundations, and umbrella organizations](https://lwn.net/Articles/561336/), LWN, 2013.
* [LFCS: The value of FOSS fiscal sponsorship](https://lwn.net/Articles/548542/), LWN, 2013.
* [Fundraising 101 from the Community Leadership Summit](https://lwn.net/Articles/560381/), LWN, 2013.
* [Yorba, the IRS, and tax-exemption](https://lwn.net/Articles/604885/), LWN, 2014.
* [X.Org mulls joining SPI](https://lwn.net/Articles/638549/), LWN, 2015.
* [Foundations and open-source projects](https://lwn.net/Articles/689596/), LWN, 2016.
* [Three new FOSS umbrella organizations in Europe](https://lwn.net/Articles/713073/), LWN, 2017.
