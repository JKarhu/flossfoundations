---
layout: single
title: "Should I Start A Non-Profit Organization (NPO) for my FLOSS Project?"
date: 2010-07-20
permalink: /starting-a-non-profit
author: gerv
categories:
    - meetings
---

Most FLOSS projects probably *could* benefit from the structure and services
that a NPO could provide. However, whether or not you need to start your own
NPO to gain these services is a different question. We'll try to address both
issues in this document.

TYPICAL BENEFITS OF NPO EXISTENCE FOR FLOSS PROJECTS

* Ability to raise funds more easily/effectively
* They can give donors a tax benefit, and avoiding taxes on funds raised
* It may help them create a governance structure
* It gives greater brand recognition
* There's too much money and associated accounting/bookkeeping/filings for one person to handle
* They are worried about contributor liability, e.g. if organising a conference
* It can hold assets (e.g. trademarks or domain names) on behalf of the project

Some of these are valid reasons for needing non-profit status, but that doesn't necessarily mean you have to start your own. In fact, you may find you are much better off working with an established NPO that can lend you expertise in many of these areas. By housing many projects within a single corporate entity, there's less burden on any one project of maintaining the corporate infrastructure and understanding changing legal nuances.

Who could I work with?

There are many organizations which exist purely to accept FLOSS project as
members or other types of affiliates so that your FLOSS project can get the
benefits of having a NPO without creating your own NPO. These organizations
are sometimes called "umbrella organizations". Another common name is "fiscal
sponsor". (FIXME: Maybe this should be a footnote? Fiscal sponsor is more of a
"non-profit geeks" term, and is sometimes confusing, since people assume a
"fiscal sponsor" means that the organization *gives* you money. Typically, it
is merely an organization that *holds* and *oversees* the projects money for you,
and may or may not help you actually raise the money).

* Software Freedom Conservancy

Organizational Structure:

Software Freedom Conservancy (Conservancy) is an organization that exists
primarily to be the umbrella organization for its member projects. As such,
it is effectively the composition of its of member projects. When a member
project joins the Conservancy, it becomes part of the Conservancy. (Although
it's not a perfect analogy, the relationship between the project and the
Conservancy is roughly similar to a division of a company.)

Conservancy's management oversees, monitors and handles all payments, invoicing,
and collects all donations that are made to the project. However, all actions
that the Conservancy takes on behalf of the project are recommended and/or
approved by the project's leadership.

The FLOSS project maintains all control to make all technical decisions

Criteria:

Services:

* SPI (Software in the Public Interest)

Organization Structure: SPI is a non-profit which was founded to help organizations
develop and distribute open hardware and software.

* Apache

* Free Software Foundation

* LinuxFund

If you still feel that you need your own non-profit, there are many things to bear in mind.
For an overview of many of these topics, see the excellent primer published by the Software Freedom
Law Center at http://www.softwarefreedom.org/resources/2008/foss-primer.html#x1-190003

In the US, there are two IRS classifications which non-profit organizations in the free software space have registered under. Most common is the 501(c)3, but also there's the 501(c)6. Stormy Peters wrote a short article on the differences between the two: http://stormyscorner.com/2008/08/501c-3-versus-6.html

Philosophically, a 501(c)3 is set up for the benefit of the public, whereas a 501(c)6 is set up for the benefit of its members. Therefore, in the (c)6 case, your governance structure and who becomes a member is correspondingly more important.

Incorporation?

International...
