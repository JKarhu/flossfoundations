---
layout: single
title: "Best of the Foundations mailing list"
date: 2008-07-19
permalink: /best-foundations-mailing-list
author: dneary
---

Over the years, many subjects have come up on the foundations mailing list which have resulted in discussion, argument and shared experiences. This page is a link to the origin of those mailing list discussions in [the archives](http://lists.freedesktop.org/mailman/private/foundations/), with the general topic of the discussion. Ideally, we would have pages summarising these discussions into a coherent whole. We could turn it into a book ;) This list is up to date as of April 2008. Some of the most interesting discussions that have come up since the list started are listed below, with links to mailing list posts.

* [FLOSS based course material for universities](http://lists.freedesktop.org/mailman/private/foundations/2008-April/001502.html)
* [Interpretations of the GPL](http://lists.freedesktop.org/mailman/private/foundations/2008-March/001409.html)
* [Assigning/sharing copyright](http://lists.freedesktop.org/mailman/private/foundations/2008-March/001425.html)
* [The role of unelected officers in the foundation](http://lists.freedesktop.org/mailman/private/foundations/2008-February/001381.html)
* [Directors & Officers insurance](http://lists.freedesktop.org/mailman/private/foundations/2008-January/001358.html)
* [Managing languages in documentation](http://lists.freedesktop.org/mailman/private/foundations/2007-October/001305.html)
* [Trademarks](http://lists.freedesktop.org/mailman/private/foundations/2007-August/001171.html) - [again](http://lists.freedesktop.org/mailman/private/foundations/2007-July/001012.html) and [again](http://lists.freedesktop.org/mailman/private/foundations/2006-September/000705.html) and [again](http://lists.freedesktop.org/mailman/private/foundations/2006-May/000500.html) and [again](http://lists.freedesktop.org/mailman/private/foundations/2005-September/000103.html)
* [Donor management software](http://lists.freedesktop.org/mailman/private/foundations/2007-August/001252.html) [(and again)](http://lists.freedesktop.org/mailman/private/foundations/2006-July/000593.html)
* [Who's on the list? (as of July 2007)](http://lists.freedesktop.org/mailman/private/foundations/2007-July/001078.html)
* [Choosing a bank](http://lists.freedesktop.org/mailman/private/foundations/2007-May/000961.html)
* [Community grants](http://lists.freedesktop.org/mailman/private/foundations/2007-March/000929.html) [(and again)](http://lists.freedesktop.org/mailman/private/foundations/2006-August/000669.html)
* [IRS 501c stuff](http://lists.freedesktop.org/mailman/private/foundations/2007-February/000875.html)
* [Email voting](http://lists.freedesktop.org/mailman/private/foundations/2006-October/000737.html) [(and again)](http://lists.freedesktop.org/mailman/private/foundations/2006-November/000795.html)
* [Does an association & revenues undermine community?](http://lists.freedesktop.org/mailman/private/foundations/2006-October/000746.html)
* [Executive director or not?](http://lists.freedesktop.org/mailman/private/foundations/2006-August/000654.html)
* [Best practices for running conferences](http://lists.freedesktop.org/mailman/private/foundations/2006-July/000614.html)
* [Alternatives to forming a non-profit](http://lists.freedesktop.org/mailman/private/foundations/2006-March/000401.html) [(and again)](http://lists.freedesktop.org/mailman/private/foundations/2005-August/000075.html)
