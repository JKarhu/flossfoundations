---
layout: single
title: "FLOSS Foundations at SCaLE 2009"
date: 2009-02-20
permalink: /floss-foundations-scale-2009
categories:
    - meetings
---

The face-to-face meetings are a place for the people who try to Get Stuff Done at various open source projects and non-profits around open source to congregate, share information, and pick each others' brains.

## When and Where:

Friday, February 20th, [Southern California Linux Expo](http://web.archive.org/web/20090221165347/http://scale7x.socallinuxexpo.org/) (SCaLE), Westin Hotel, National Room, 7PM - 9PM

Attendee Layout, in a circle:

* Justin Erenkrantz, Apache Software Foundation
* Bradley Kuhn, Software Freedom Conservancy/Software Freedom Law Center
* Selena Deckelmann, Open Source Bridge Foundation
* Unknown, from Women In Open Source session
* Josh Berkus, Core Team, PostgreSQL & Assistant Treasurer, SPI
* Stuart Sheldon, Linux Expo of Southern California (nonprofit that puts on SCaLE)
* Leslie Hawthorn, Google
* Catherine "Cat" Allman, Google
* Stormy Peters, GNOME Foundation
* Michael Dexter, Linux Fund/BSD Fund
* Scott Rainey, Linux Fund

Am I missing anyone?

## Topics:

* Donation Payment Processors
* FLOSS Foundations Directory
* Open Discussion

## Donation Payment Processors

Moved to: http://www.flossfoundations.org/online-payment-processors

## FLOSS Foundations Directory

Q: Terminology: Foundation? NGO? Organization?

Possible criteria: An organization with a mission to support one or more open source software projects - should serve hackers

Possible disqualification: Supports proprietary software as in the case of TechSoup.

Open Discussion
Challenge: ASF: Public support test with large donations

ASF admires the Eclipse Foundation model and is proud not to have an Executive Director. What does the IRS say?

Q: How do we get developers to donate? Give with code?
Conference orgs help limit liability and justify concern for money and urgency.

Observation: High need for conservancies to prevent creation of too many tiny orgs and the fact that the FSC is back-logged.

$5,000 to $10,000 an awkward amount for organizations

Be prepared for transparency if you start and org.

Several orgs have money but no people or procedures to handle it.

Some orgs are nervous about money and all of its implications.

Many developers do not trust non-developers/suits to take responsibility for non-code aspects of their projects.

Perhaps the [Peter Principle](http://en.wikipedia.org/wiki/Peter_Principle) is at work in many cases?

PG has 8 orgs!

Orgs make it difficult to shut down projects through legal action.

Orgs are difficult to maintain in Japan.

Model: SPI: A bank account.

Model: SFC: Legal structure.

Model: ASF: Hybrid?

Risk: Does a dated deliverable qualify as a commercial work for hire?

Thank you all who attended. Please contribute your notes as appropriate.
